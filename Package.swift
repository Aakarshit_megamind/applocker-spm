// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AppLocker-SPM",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "AppLocker-SPM",
            targets: ["AppLocker-SPM"]),
    ],
    dependencies: [
        .package(url: "https://github.com/square/Valet.git", .upToNextMajor(from: "4.2.0"))],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "AppLocker-SPM",
            dependencies:[.product(name: "Valet", package: "Valet")]
        ),
        .testTarget(
            name: "AppLocker-SPMTests",
            dependencies: ["AppLocker-SPM"]),
    ]
)
